package com.drh.games.service;

import com.drh.games.model.Player;
import com.drh.games.model.PlayerAction;
import com.drh.games.model.TableRules;
import com.drh.games.util.PlayerActionParser;
import java.util.List;
import java.util.Scanner;

public class TerminalPlayerActionRequester implements PlayerActionRequester {

  private static TerminalPlayerActionRequester instance;

  private final Scanner scanner;

  private TerminalPlayerActionRequester() {
    scanner = new Scanner(System.in);
  }

  public static TerminalPlayerActionRequester instance() {
    if (instance == null) {
      instance = new TerminalPlayerActionRequester();
    }
    return instance;
  }

  @Override
  public PlayerAction requestAction(
      List<PlayerAction> prevActions,
      Player player,
      TableRules tableRules,
      boolean isPreFlop) {

    do {
      System.out.print(player.getName() + ": ");
      String line = scanner.nextLine().trim().toLowerCase();
      try {
        return PlayerActionParser.parseAndValidateAction(line, prevActions, player, tableRules, isPreFlop);
      } catch (IllegalArgumentException e) {
        System.out.println("ERROR: " + e.getMessage() + ", try again");
      }
    } while (true);
  }
}
