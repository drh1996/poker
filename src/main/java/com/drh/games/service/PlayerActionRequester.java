package com.drh.games.service;

import com.drh.games.model.Player;
import com.drh.games.model.PlayerAction;
import com.drh.games.model.TableRules;
import java.util.List;

public interface PlayerActionRequester {

  PlayerAction requestAction(
      List<PlayerAction> prevActions,
      Player player,
      TableRules tableRules,
      boolean isPreFlop);
}
