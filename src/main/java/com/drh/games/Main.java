package com.drh.games;

import com.drh.games.model.Player;
import com.drh.games.model.Table;

public class Main {

  public static void main(String[] args) {
    Table t = new Table();
    t.addPlayers(
        new Player("A", 0),
        new Player("B", 1),
        new Player("C", 2),
        new Player("D", 3),
        new Player("E", 4));

    t.startHand();
  }
}