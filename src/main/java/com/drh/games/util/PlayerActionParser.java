package com.drh.games.util;

import com.drh.games.model.Player;
import com.drh.games.model.PlayerAction;
import com.drh.games.model.TableRules;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlayerActionParser {

  private static final Pattern ACTION_RE_PATTERN = Pattern.compile("([a-z ]+)\\s?(\\d+)?");

  public static PlayerAction parseAndValidateAction(
      String src,
      List<PlayerAction> prevActions,
      Player player,
      TableRules tableRules,
      boolean isPreFlop) {

    Matcher m = ACTION_RE_PATTERN.matcher(src.toLowerCase());

    if (!m.matches()) {
      throw new IllegalArgumentException("Invalid action provided '" + src + '\'');
    }

    String actionStr = m.group(1).trim();
    String chipsStr = m.group(2);
    if (chipsStr != null) {
      chipsStr = chipsStr.trim();
    }

    int action;
    boolean needsChips = false;
    switch (actionStr) {
      case "fold":
        action = PlayerAction.FOLD;
        break;
      case "check":
        action = PlayerAction.CHECK;
        break;
      case "call":
        action = PlayerAction.CALL;
        break;
      case "all in":
        action = PlayerAction.ALL_IN;
        break;
      case "bet":
        action = PlayerAction.BET;
        needsChips = true;
        break;
      case "raise":
        action = PlayerAction.RAISE;
        needsChips = true;
        break;
      default:
        throw new IllegalArgumentException("Player action '" + actionStr + "' is not valid");
    }

    if (needsChips && chipsStr == null) {
      throw new IllegalArgumentException("No chips provided for " + actionStr);
    }

    PlayerAction lastNonFoldAction = getLastNonFoldPlayerAction(prevActions);

    validateAction(action, player, prevActions, lastNonFoldAction, isPreFlop);

    int chips = 0;
    if (needsChips) {
      chips = getChips(chipsStr);

      validateChips(action, chips, player, prevActions, tableRules);

      // if bet / raise is all your chips change action to all in
      if (chips == player.getChips()) {
        action = PlayerAction.ALL_IN;
      }
    }

    if (action == PlayerAction.CALL && lastNonFoldAction != null) {
      chips = Math.min(player.getChips(), lastNonFoldAction.getChips());
    }

    if (action == PlayerAction.ALL_IN) {
      chips = player.getChips();
    }

    return new PlayerAction(player, action, chips);
  }

  private static void validateAction(int action, Player player, List<PlayerAction> prevActions, PlayerAction lastNonFoldAction, boolean isPreFlop) {

    //first to act can fold, check, bet, & all in (post-flop)
    if (!isPreFlop && (prevActions.isEmpty() || lastNonFoldAction == null)) {
      if (action == PlayerAction.CALL || action == PlayerAction.RAISE) {
        throw new IllegalArgumentException(
            "First to act cannot " + PlayerAction.actionString(action));
      }
    }

    //can fold or all in any time
    if (action == PlayerAction.FOLD || action == PlayerAction.ALL_IN) {
      return;
    }

    //can only check if prev action was also check
    if (action == PlayerAction.CHECK) {
      int prevAction = lastNonFoldAction != null ? lastNonFoldAction.getAction() : PlayerAction.CHECK;

      // can only check if its preflop, you are the big blind, and the last action was a call to your blind
      if (isPreFlop && prevAction == PlayerAction.CALL && player.isBigBlind()
          && lastNonFoldAction.getChips() == player.getCurrentBet()) {
        return;
      } else if (prevAction != PlayerAction.CHECK) {
        throw new IllegalArgumentException(
            "You cannot check because the previous player " + PlayerAction.actionStringPastTense(prevAction));
      }
    }

    // can only call if prev action was blind, call, bet, raise, or all in
    if (action == PlayerAction.CALL) {
      int prevAction = lastNonFoldAction.getAction();
      if (prevAction == PlayerAction.CHECK) {
        throw new IllegalArgumentException("You cannot call because the previous player checked");
      }
    }

    // can only bet if prev action is check
    if (action == PlayerAction.BET) {
      int prevAction = lastNonFoldAction != null ? lastNonFoldAction.getAction() : PlayerAction.CHECK;
      if (prevAction != PlayerAction.CHECK) {
        throw new IllegalArgumentException(
            "You cannot bet because the previous player " + PlayerAction.actionStringPastTense(prevAction));
      }
    }

    // can only raise if prev action was blind, call, bet, raise, or all in
    if (action == PlayerAction.RAISE) {
      int prevAction = lastNonFoldAction.getAction();
      if (prevAction == PlayerAction.CHECK) {
        throw new IllegalArgumentException("You cannot raise because the previous player checked");
      }
    }
  }

  //TODO does not account for a prev all in less than another prev bet/raise/all in
  //TODO: nerd shit: https://poker.stackexchange.com/questions/2729/what-is-the-min-raise-and-min-reraise-in-holdem-no-limit
  private static void validateChips(int action, int chips, Player player, List<PlayerAction> prevActions, TableRules tableRules) {
    // cannot bet / raise more than u have
    if (chips > player.getChips()) {
      throw new IllegalArgumentException("You only have " + player.getChips() + " chips remaining");
    }

    // cannot bet / raise less than the big blind
    if (chips < tableRules.getBigBlind()) {
      throw new IllegalArgumentException(
          "Cannot " + PlayerAction.actionString(action) + " " + chips + " because it's less than the minimum (" + tableRules.getBigBlind() + ")");
    }

    // cannot raise less than min raise
    //TODO: this does not account for all types of raising, this just doubles the prev raise or bet
    if (action == PlayerAction.RAISE) {
      //not possible to be null as validateAction checks player cant raise without a prev bet, raise, or all in
      PlayerAction prevAction = getLastActionWithChipsUsed(prevActions);
      int minRaise = prevAction.getChips() * 2;

      if (chips < minRaise) {
        throw new IllegalArgumentException("Cannot raise less than the minimum " + minRaise);
      }
    }
  }

  private static PlayerAction getLastNonFoldPlayerAction(List<PlayerAction> playerActions) {
    for (int i = playerActions.size() - 1; i >= 0; i--) {
      PlayerAction action = playerActions.get(i);
      if (action.getAction() != PlayerAction.FOLD) {
        return action;
      }
    }
    return null;
  }

  private static PlayerAction getLastActionWithChipsUsed(List<PlayerAction> playerActions) {
    for (int i = playerActions.size() - 1; i >= 0; i--) {
      PlayerAction action = playerActions.get(i);
      if (action.getAction() != PlayerAction.FOLD && action.getAction() != PlayerAction.CHECK) {
        return action;
      }
    }
    return null;
  }

  private static int getChips(String chips) {
    try {
      return Integer.parseInt(chips);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Chips provided are not valid '" + chips + '\'');
    }
  }
}
