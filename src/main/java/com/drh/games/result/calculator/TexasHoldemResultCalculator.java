package com.drh.games.result.calculator;

import com.drh.games.model.Card;
import com.drh.games.model.Hand;
import com.drh.games.result.Result;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TexasHoldemResultCalculator {

  private static final int MAX_RESULT_CARDS_SIZE = 5;

  public static Result calculate(Hand hand, Card[] cards) {
    // get all cards ordered from ace -> king
    final List<Card> cardList = toList(hand, cards);

    // straight flush
    List<Card> flushCards = getFlushCards(cardList);
    if (flushCards != null) {
      List<Card> straightFlush = hasStraight(flushCards);
      if (straightFlush != null) {
        return new Result(getStraightFlushType(straightFlush), straightFlush);
      }
    }

    List<List<Card>> valueGroups = groupByValue(cardList);

    // four of a kind
    Result fourKind = hasFourKind(valueGroups, cardList);
    if (fourKind != null) {
      return fourKind;
    }

    // full house
    List<Card> threeKind = getHighestThreeKind(valueGroups);
    if (threeKind != null && valueGroups.size() > 1) {
      List<Card> nextPair = getHighestPairExcluding(valueGroups, getValue(threeKind, 0));
      if (nextPair != null) {
        threeKind.addAll(nextPair);
        return new Result(Result.FULL_HOUSE, threeKind);
      }
    }

    // flush
    if (flushCards != null) {
      return getHighestFlush(flushCards);
    }

    // straight
    List<Card> straight = hasStraight(cardList);
    if (straight != null) {
      return new Result(Result.STRAIGHT, straight);
    }

    // three of a kind
    if (threeKind != null) {
      List<Card> high = getHighCardsExcluding(cardList, 2, getValue(threeKind, 0));
      threeKind.addAll(high);
      return new Result(Result.THREE_OF_A_KIND, threeKind);
    }

    // two pair
    if (valueGroups.size() >= 2) {
      List<Card> twoPair = getHighestTwoPair(valueGroups);
      twoPair.add(getHighCardExcluding(
          cardList, getValue(twoPair, 0), getValue(twoPair, twoPair.size() - 1)));
      return new Result(Result.TWO_PAIR, twoPair);
    }

    // pair
    if (valueGroups.size() == 1) {
      List<Card> pair = valueGroups.get(0);
      pair.addAll(getHighCardsExcluding(cardList, 3, getValue(pair, 0)));
      return new Result(Result.PAIR, pair);
    }

    // high card
    return new Result(Result.HIGH_CARD, getHighestCards(cardList));
  }

  public static Result hasFourKind(List<List<Card>> valueGroups, List<Card> cards) {
    int highestValue = 0;
    List<Card> cardList = null;
    for (List<Card> valueList : valueGroups) {
      int value = getValue(valueList, 0);
      if (valueList.size() == 4) {
        if (value > highestValue || value == Card.ACE) {
          cardList = valueList;
          highestValue = value;
          if (value == Card.ACE) {
            break;
          }
        }
      }
    }

    if (cardList != null) {
      cardList.add(getHighCardExcluding(cards, highestValue));
      return new Result(Result.FOUR_OF_A_KIND, cardList);
    }

    return null;
  }

  public static List<Card> getHighestThreeKind(List<List<Card>> valueGroups) {
    int highestValue = 0;
    List<Card> cardList = null;
    for (List<Card> valueList : valueGroups) {
      int value = getValue(valueList, 0);
      if (valueList.size() == 3) {
        if (value == Card.ACE) {
          return valueList;
        }
        if (value > highestValue) {
          cardList = valueList;
          highestValue = value;
        }
      }
    }
    return cardList;
  }

  private static List<Card> getHighestTwoPair(List<List<Card>> valueGroups) {
    List<Card> cardList = new ArrayList<>(4);
    if (getValue(valueGroups.get(0), 0) == Card.ACE) {
      cardList.addAll(valueGroups.get(0));
    }

    for (int i = valueGroups.size() - 1; i >= 0 && cardList.size() < 4; i--) {
      cardList.addAll(valueGroups.get(i));
    }

    return cardList;
  }

  public static List<Card> getHighestPairExcluding(List<List<Card>> valueGroup, int excludeValue) {
    int highestValue = 0;
    List<Card> cardList = null;
    for (List<Card> valueList : valueGroup) {
      int value = getValue(valueList, 0);
      if (value == excludeValue) {
        continue;
      }

      if (value == Card.ACE) {
        return valueList;
      }

      if (value > highestValue) {
        cardList = valueList;
        highestValue = value;
      }
    }

    if (cardList != null) {
      return cardList.subList(0, 2);
    }
    return null; // TODO not possible
  }

  private static Card getHighCardExcluding(List<Card> cards, int... excludeValues) {
    if (getValue(cards, 0) == Card.ACE && !contains(excludeValues, Card.ACE)) {
      return cards.get(0);
    }

    for (int i = cards.size() - 1; i >= 0; i--) {
      Card c = cards.get(i);
      if (!contains(excludeValues, c.getValue())) {
        return c;
      }
    }
    return null; //TODO not possible
  }

  private static List<Card> getHighCardsExcluding(List<Card> cards, int count, int excludeValue) {
    List<Card> cardList = new ArrayList<>(count);

    if (getValue(cards, 0) == Card.ACE && excludeValue != Card.ACE) {
      cardList.add(cards.get(0));
      count--;
    }

    final int stopIndex = cardList.size(); //don't add ace twice if less than "count" cards

    for (int i = cards.size() - 1; i >= stopIndex && count > 0; i--) {
      Card c = cards.get(i);
      if (c.getValue() != excludeValue) {
        cardList.add(c);
        count--;
      }
    }
    return cardList;
  }

  private static List<Card> getHighestCards(List<Card> cards) {
    return getHighCardsExcluding(cards, MAX_RESULT_CARDS_SIZE, -1);
  }

  public static List<Card> getFlushCards(List<Card> cards) {
    Map<Integer, List<Card>> suitMap = cards.stream().collect(Collectors.groupingBy(Card::getSuit));
    for (List<Card> cardsList : suitMap.values()) {
      if (cardsList.size() >= MAX_RESULT_CARDS_SIZE) {
        return cardsList;
      }
    }

    return null;
  }

  private static Result getHighestFlush(List<Card> flushCards) {
    int remainingCards = MAX_RESULT_CARDS_SIZE;
    List<Card> result = new ArrayList<>(MAX_RESULT_CARDS_SIZE);
    if (getValue(flushCards, 0) == Card.ACE) {
      result.add(flushCards.get(0));
      remainingCards--;
    }

    for (int i = flushCards.size() - 1; i >= 0 && remainingCards > 0; i--) {
      result.add(flushCards.get(i));
      remainingCards--;
    }

    return new Result(Result.FLUSH, result);
  }

  public static List<Card> hasStraight(List<Card> cards) {
    int offset = 4;

    List<Card> distinctValues = cards.stream()
        .filter(distinctByKey(Card::getValue))
        .collect(Collectors.toList());

    final int size = distinctValues.size();
    if (distinctValues.size() < MAX_RESULT_CARDS_SIZE) {
      return null;
    }

    // has ace high straight
    if (getValue(distinctValues, size - 1) == Card.KING
        && getValue(distinctValues, size - 4) == Card.TEN
        && getValue(distinctValues, 0) == Card.ACE) {

      List<Card> cardList = distinctValues.subList(size - 4, size);
      cardList.add(distinctValues.get(0));
      return cardList;
    }

    for (int i = size - 1; i - offset >= 0; i--) {
      int c1 = getValue(distinctValues, i);
      int c2 = getValue(distinctValues, i - offset);

      if (c1 - c2 == 4) {
        return distinctValues.subList(i - offset, i + 1);
      }
    }

    return null;
  }

  private static int getStraightFlushType(List<Card> cards) {
    final int firstValue = getValue(cards, 0);
    final int lastValue = getValue(cards, cards.size() - 1);

    return firstValue == Card.TEN && lastValue == Card.ACE
        ? Result.ROYAL_FLUSH : Result.STRAIGHT_FLUSH;
  }

  public static List<List<Card>> groupByValue(List<Card> cards) {
    Map<Integer, List<Card>> valueMap = new HashMap<>();
    for (Card c : cards) {
      Integer value = c.getValue();
      if (valueMap.containsKey(value)) {
        valueMap.get(value).add(c);
      } else {
        ArrayList<Card> cardList = new ArrayList<>(4);
        cardList.add(c);
        valueMap.put(value, cardList);
      }
    }

    List<List<Card>> cardsList = new ArrayList<>(valueMap.size());
    for (Map.Entry<Integer, List<Card>> entry : valueMap.entrySet()) {
      List<Card> cs = entry.getValue();
      if (cs.size() >= 2) {
        cardsList.add(cs);
      }
    }
    cardsList.sort(Comparator.comparing(List::size));
    return cardsList;
  }

  public static List<Card> toList(Hand hand, Card[] cards) {
    List<Card> cardsList = new ArrayList<>(cards.length + hand.getCards().length);
    Collections.addAll(cardsList, cards);
    Collections.addAll(cardsList, hand.getCards());

    cardsList.sort(Comparator.comparing(Card::getValue));
    return cardsList;
  }

  private static boolean contains(int[] arr, int value) {
    for (int i : arr) {
      if (i == value) {
        return true;
      }
    }
    return false;
  }

  private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = new HashSet<>();
    return t -> seen.add(keyExtractor.apply(t));
  }

  private static int getValue(List<Card> cards, int index) {
    return cards.get(index).getValue();
  }

}
