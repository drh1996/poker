package com.drh.games.result.util;

import com.drh.games.model.Card;
import com.drh.games.result.Result;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResultStringUtils {

  public static String getRankingString(int ranking) {
    switch (ranking) {
      case Result.ROYAL_FLUSH: return "Royal Flush";
      case Result.STRAIGHT_FLUSH: return "Straight Flush";
      case Result.FOUR_OF_A_KIND: return "Four Of A Kind";
      case Result.FULL_HOUSE: return "Full House";
      case Result.FLUSH: return "Flush";
      case Result.STRAIGHT: return "Straight";
      case Result.THREE_OF_A_KIND: return "Three Of A Kind";
      case Result.TWO_PAIR: return "Two Pair";
      case Result.PAIR: return "Pair";
      case Result.HIGH_CARD: return "High Card";
      default: throw new IllegalArgumentException("Invalid ranking '" + ranking + '\'');
    }
  }

  public static String getRankingVerbose(int ranking, List<Card> cards) {
    switch (ranking) {
      case Result.ROYAL_FLUSH: return "Royal Flush";
      case Result.STRAIGHT_FLUSH: return getStraightCardString("Straight Flush %s to %s", cards);
      case Result.FOUR_OF_A_KIND: return getFourKindString(cards);
      case Result.FULL_HOUSE: return getFullHouseString(cards);
      case Result.STRAIGHT: return getStraightCardString("Straight %s to %s", cards);
      case Result.FLUSH: return getFlushString(cards);
      case Result.THREE_OF_A_KIND: return getThreeKindString(cards);
      case Result.TWO_PAIR: return getTwoPairString(cards);
      case Result.PAIR: return getPairString(cards);
      case Result.HIGH_CARD: return getHighCardString(cards);
      default: throw new IllegalArgumentException("Invalid ranking '" + ranking + '\'');
    }
  }

  private static String getHighCardString(List<Card> cards) {
    return getFirstValue(cards) + " High";
  }

  private static String getPairString(List<Card> cards) {
    return "Pair of " + getFirstValue(cards) + 's';
  }

  private static String getTwoPairString(List<Card> cards) {
    return "Two Pair " + getFirstValue(cards) + "s and " + getValue(cards, 2) + 's';
  }

  private static String getThreeKindString(List<Card> cards) {
    return "Three Of A Kind " + getFirstValue(cards) + 's';
  }

  private static String getFlushString(List<Card> cards) {
    return getHighCardString(cards) + " Flush";
  }

  private static String getFullHouseString(List<Card> cards) {
    return "Full House " + getFirstValue(cards) + "s and " + getValue(cards, 3) + 's';
  }

  private static String getFourKindString(List<Card> cards) {
    return "Four Of A Kind " + getFirstValue(cards) + 's';
  }

  private static String getStraightCardString(String s, List<Card> cards) {
    return String.format(s, getFirstValue(cards), getLastValue(cards));
  }

  private static String getValue(List<Card> cards, int index) {
    return cards.get(index).getValueString();
  }

  private static String getFirstValue(List<Card> cards) {
    return getValue(cards, 0);
  }

  private static String getLastValue(List<Card> cards) {
    return
        getValue(cards, cards.size() - 1);
  }
}
