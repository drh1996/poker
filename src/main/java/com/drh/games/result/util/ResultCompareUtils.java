package com.drh.games.result.util;

import com.drh.games.model.Card;
import com.drh.games.result.Result;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ResultCompareUtils {

  public static int compare(Result left, Result right) {
    int res = Integer.compare(left.getRanking(), right.getRanking());
    if (res != 0) {
      return res;
    }

    switch (left.getRanking()) {
      case Result.ROYAL_FLUSH:
        return 0;
      case Result.STRAIGHT:
      case Result.STRAIGHT_FLUSH:
        return compareStraight(left.getCards(), right.getCards());
      case Result.FLUSH:
      case Result.PAIR:
      case Result.TWO_PAIR:
      case Result.THREE_OF_A_KIND:
      case Result.FULL_HOUSE:
      case Result.FOUR_OF_A_KIND:
      case Result.HIGH_CARD:
      default:
        return compareCards(left.getCards(), right.getCards());
    }
  }

  private static int compareStraight(List<Card> left, List<Card> right) {
    Card leftCard = left.get(0);
    Card rightCard = right.get(0);

    //needed for if ace is first (so ace < 2 for lower straights e.g. (A2345 < 23456))
    int res = Integer.compare(leftCard.getValue(), rightCard.getValue());
//    if (res != 0) {
//      return res;
//    }
    return res;
// TODO not needed, above checks lowest card for who has highest straight
//    for (int i = 1; i < left.size(); i++) {
//      leftCard = left.get(i);
//      rightCard = right.get(i);
//
//      res = leftCard.compareTo(rightCard);
//      if (res != 0) {
//        return res;
//      }
//    }
//    return 0;
  }

  private static int compareCards(List<Card> left, List<Card> right) {
    for (int i = 0; i < left.size(); i++) {
      Card leftCard = left.get(i);
      Card rightCard = right.get(i);

      int res = leftCard.compareTo(rightCard);
      if (res != 0) {
        return res;
      }
    }
    return 0;
  }
}
