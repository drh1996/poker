package com.drh.games.result;

import com.drh.games.model.Card;
import com.drh.games.model.GameType;
import com.drh.games.model.Hand;
import com.drh.games.result.calculator.TexasHoldemResultCalculator;
import com.drh.games.result.util.ResultCompareUtils;
import com.drh.games.result.util.ResultStringUtils;
import java.util.List;
import java.util.Objects;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Result implements Comparable<Result> {

  public static final int HIGH_CARD = 0;
  public static final int PAIR = 1;
  public static final int TWO_PAIR = 2;
  public static final int THREE_OF_A_KIND = 3;
  public static final int STRAIGHT = 4;
  public static final int FLUSH = 5;
  public static final int FULL_HOUSE = 6;
  public static final int FOUR_OF_A_KIND = 7;
  public static final int STRAIGHT_FLUSH = 8;
  public static final int ROYAL_FLUSH = 9;

  private final int ranking;
  private final List<Card> cards;

  public String getRankingString() {
    return ResultStringUtils.getRankingString(ranking);
  }

  public String getRankingVerbose() {
    return ResultStringUtils.getRankingVerbose(ranking, cards);
  }

  @Override
  public int compareTo(Result other) {
    return ResultCompareUtils.compare(this, other);
  }

  //TODO handle rules for other poker games other than texas hold'em
  public static Result calculate(Hand hand, Card[] cards, GameType gameType) {
    Objects.requireNonNull(hand, "hand cannot be null");
    Objects.requireNonNull(cards, "cards cannot be null");
    Objects.requireNonNull(gameType, "gameType cannot be null");
    switch (gameType) {
      case TEXAS_HOLDEM: return TexasHoldemResultCalculator.calculate(hand, cards);
      default:
        throw new UnsupportedOperationException("Unsupported Game Type: " + gameType);
    }
  }
}
