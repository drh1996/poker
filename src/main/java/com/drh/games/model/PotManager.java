package com.drh.games.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;

@Getter
public class PotManager {

  private final Pot mainPot;
  private final List<Pot> sidePots;

  public PotManager() {
    this.mainPot = new Pot();
    this.sidePots = new ArrayList<>();
  }

  public void addChipsToPot(List<Player> playerList) {
    List<Player> players = playerList.stream()
        .filter(player -> player.getCurrentBet() > 0)
        .sorted(Comparator.comparing(Player::getCurrentBet))
        .collect(Collectors.toList());

    for (Player player : players) {
      if (mainPot.isLocked()) {
        addChipsToSidePot(player.getCurrentBet(), player, 0);
        continue;
      }

      if (mainPot.getMaxChipsPerPlayer() == 0) {
        if (player.isAllIn()) {
          mainPot.setMaxChipsPerPlayer(player.getCurrentBet());
        }
        mainPot.addChips(player.getCurrentBet(), player);
      } else {
        int remainder = player.getCurrentBet() - mainPot.getMaxChipsPerPlayer();
        if (remainder > 0) {
          mainPot.addChips(mainPot.getMaxChipsPerPlayer(), player);
          addChipsToSidePot(remainder, player, 0);
        } else {
          mainPot.addChips(player.getCurrentBet(), player);
        }
      }
    }

    lockPotsIfNeeded();
  }

  private void addChipsToSidePot(int remainingChips, Player player, int sidePotIndex) {
    Pot pot;
    if (sidePotIndex == sidePots.size()) {
      pot = new Pot();
      sidePots.add(pot);
    } else {
      pot = sidePots.get(sidePotIndex);
      if (pot.isLocked()) {
        addChipsToSidePot(remainingChips, player, sidePotIndex + 1);
        return;
      }
    }

    if (pot.getMaxChipsPerPlayer() == 0) {
      if (player.isAllIn()) {
        pot.setMaxChipsPerPlayer(remainingChips);
      }
      pot.addChips(remainingChips, player);
    } else {
      int remainder = remainingChips - pot.getMaxChipsPerPlayer();
      if (remainder > 0) {
        pot.addChips(pot.getMaxChipsPerPlayer(), player);
        addChipsToSidePot(remainder, player, sidePotIndex + 1);
      } else {
        pot.addChips(remainingChips, player);
      }
    }
  }

  private void lockPotsIfNeeded() {
    if (mainPot.getMaxChipsPerPlayer() > 0) {
      mainPot.setLocked(true);
    }

    for (Pot pot : sidePots) {
      if (pot.getMaxChipsPerPlayer() > 0) {
        pot.setLocked(true);
      }
    }
  }

  public void reset() {
    this.mainPot.reset();
    this.sidePots.clear();
  }

  public boolean hasSidePot() {
    return sidePotsCount() != 0;
  }

  public int sidePotsCount() {
    return this.sidePots.size();
  }

  @Override
  public String toString() {
    String res = "main pot: " + mainPot.getChips();
    if (!sidePots.isEmpty()) {
      res += ", side pots: ";
      res += sidePots.stream()
          .map(pot -> pot.getChips() + " (" + pot.getPlayers() + ")")
          .reduce((prev, next) -> prev + ", " + next)
          .orElse("None");
    }
    return res;
  }
}
