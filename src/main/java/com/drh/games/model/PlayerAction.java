package com.drh.games.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PlayerAction {
  public static final int BLIND  = 0;
  public static final int FOLD   = 1;
  public static final int CHECK  = 2;
  public static final int CALL   = 3;
  public static final int BET    = 4;
  public static final int RAISE  = 5;
  public static final int ALL_IN = 6;
  public static final int ALL_IN_UNDER_MIN_RAISE = 7;

//  private static final Map<Integer, List<Integer>> nextActionMap = Map.of(
//    FOLD, List.of(FOLD, CHECK, BET, ALL_IN),
//    CHECK, List.of(FOLD, CHECK, BET, ALL_IN),
//    CALL, List.of(FOLD, CALL, RAISE, ALL_IN),
//    BET, List.of(FOLD, CALL, RAISE, ALL_IN),
//    RAISE, List.of(FOLD, CALL, RAISE, ALL_IN),
//    ALL_IN, List.of(FOLD, CALL, RAISE, ALL_IN)
//  );

  private final Player player;
  private final int action;
  private final int chips;

  public PlayerAction(Player player, int action) {
    this.player = player;
    this.action = action;
    this.chips = 0;
  }

  public static String actionString(int action) {
    switch (action) {
      case BLIND: return "blind";
      case FOLD: return "fold";
      case CHECK: return "check";
      case CALL: return "call";
      case BET: return "bet";
      case RAISE: return "raise";
      case ALL_IN:
      case ALL_IN_UNDER_MIN_RAISE: return "all in";

      default:
        throw new IllegalArgumentException("Invalid action provided '" + action + '\'');
    }
  }

  public static String actionStringPastTense(int action) {
    switch (action) {
      case FOLD: return "folded";
      case CHECK: return "checked";
      case CALL: return "called";
      case BET: return "bet";
      case RAISE: return "raised";
      case ALL_IN:
      case ALL_IN_UNDER_MIN_RAISE: return "went all in";
      default:
        throw new IllegalArgumentException("Invalid action provided '" + action + '\'');
    }
  }

  @Override
  public String toString() {
    String res = player.getName() + ": " + actionString(action);
    if (chips > 0) {
      res += " (" + chips + ")";
    }
    return res;
  }
}
