package com.drh.games.model;

import com.drh.games.result.Result;
import com.drh.games.service.PlayerActionRequester;
import com.drh.games.service.TerminalPlayerActionRequester;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Table {

  private static final int PRE_FLOP = 0;
  private static final int FLOP     = 1;
  private static final int TURN     = 2;
  private static final int RIVER    = 3;

  private final PlayerActionRequester playerActionRequester;

  private GameType gameType;
  private List<Player> players;
  private List<Player> playersInHand;
  private Deck deck;
  private List<Card> board;
  private int buttonPos;
  private int actionPos;
  private PotManager potManager;
  private TableRules tableRules;

  public Table() {
    this.gameType = GameType.TEXAS_HOLDEM;
    this.players = new ArrayList<>();
    this.deck = new Deck();
    this.board = new ArrayList<>(5);
    this. buttonPos = this.actionPos = 0;
    this.potManager = new PotManager();
    this.tableRules = new TableRules(2, 1);

    this.playerActionRequester = TerminalPlayerActionRequester.instance();
  }

  public void addPlayers(Player... players) {
    final int newPlayerCount = this.players.size() + players.length;
    if (newPlayerCount > gameType.getMaxPlayersPerTable()) {
      throw new IllegalArgumentException("Too many players, max=" + gameType.getMaxPlayersPerTable()
          + ", current=" + newPlayerCount);
    }
    Collections.addAll(this.players, players);
  }

  public void startHand() {
    Map<Pot, List<Player>> winners = new HashMap<>();
    Map<Integer, List<PlayerAction>> playerActionsMap = Map.of(
        PRE_FLOP, new ArrayList<>(),
        FLOP, new ArrayList<>(),
        TURN, new ArrayList<>(),
        RIVER, new ArrayList<>());

    try {
      players.get(buttonPos).addFlag(Player.BUTTON);
      assignBlinds(playerActionsMap.get(PRE_FLOP));
      dealCards();

      for (int stage = PRE_FLOP; stage <= RIVER; stage++) {
        if (stage == FLOP) {
          addCardsToBoard(3);
          resetPlayerBets();
        } else if (stage != PRE_FLOP) {
          addCardsToBoard(1);
          resetPlayerBets();
        }

        System.out.println(getStageString(stage) + " action begins!");
        boolean hasWinner = startAction(playerActionsMap.get(stage), stage == PRE_FLOP);

        if (hasWinner) {
          winners = determineWinners();
          break;
        }
      }

      if (!winners.isEmpty()) {
        winners = determineWinners();
      }
      dividePotWinnings(winners);
    } finally {
      dumpHand(playerActionsMap, winners);
    }

    endHand();
  }

  private void endHand() {
    playersInHand = new ArrayList<>(players);
    potManager.reset();
    for (Player player : players) {
      player.resetBet();
      player.resetFlags();
    }
    buttonPos = increment(buttonPos, 1);
  }

  private void assignBlinds(List<PlayerAction> actions) {
    int sbIndex;

    if (players.size() == 2) {
      sbIndex = buttonPos;
    } else {
      sbIndex = increment(buttonPos, 1);
    }

    int bbIndex = increment(sbIndex, 1);

    Player sbPlayer = players.get(sbIndex);
    Player bbPlayer = players.get(bbIndex);

    int smallBlind = sbPlayer.paySmallBlind(tableRules.getSmallBlind());
    actions.add(new PlayerAction(sbPlayer, PlayerAction.BLIND, smallBlind));
    int bigBlind = bbPlayer.payBigBlind(tableRules.getBigBlind());
    actions.add(new PlayerAction(bbPlayer, PlayerAction.BLIND, bigBlind));
  }

  private boolean startAction(List<PlayerAction> actions, boolean isPreFlop) {
    int lastToAct = isPreFlop ? increment(buttonPos, 2) : buttonPos;
    actionPos = increment(lastToAct, 1);

    boolean isLast = false;
    do {
      if (lastToAct == actionPos) {
        isLast = true;
      }

      printGameState();

      Player player = playersInHand.get(actionPos);
      PlayerAction action = playerActionRequester.requestAction(actions, player, tableRules, isPreFlop);

      actions.add(action);

      int incBy = 1;
      switch (action.getAction()) {
        case PlayerAction.FOLD:
          playersInHand.remove(actionPos);
          player.setFolded(true);
          incBy = 0;
          if (playersInHand.size() == 1) {
            isLast = true;
          }
          if (actionPos < lastToAct) {
            lastToAct--;
          }
          break;
        case PlayerAction.CHECK:
          break;
        case PlayerAction.CALL:
          player.setCurrentBet(action.getChips());
          break;
        case PlayerAction.BET:
        case PlayerAction.RAISE:
        case PlayerAction.ALL_IN:
        case PlayerAction.ALL_IN_UNDER_MIN_RAISE:
          lastToAct = increment(actionPos, -1);
          player.setCurrentBet(action.getChips());
          isLast = false;
          break;
      }

      actionPos = increment(actionPos, incBy);

    } while (!isLast);

    potManager.addChipsToPot(players);

    return playersInHand.size() < 2;
  }

  private void dealCards() {
    deck.shuffle();
    for (Player player : playersInHand) {
      player.setHand(deck.getCards(gameType));
    }
  }

  private void addCardsToBoard(int count) {
    for (int i = 0; i < count; i++) {
      board.add(deck.getCard());
    }
  }

  private void resetPlayerBets() {
    for (Player player : players) {
      player.resetBet();
    }
  }

  private void dividePotWinnings(Map<Pot, List<Player>> potWinnersMap) {
    potWinnersMap.forEach((pot, winners) -> {
      int baseWinnings = pot.getChips() / winners.size();
      int remainingWinnings = pot.getChips() % winners.size();

      for (Player player : winners) {
        if (player.isFolded()) {
          continue;
        }

        player.addChips(baseWinnings);
      }

      if (remainingWinnings > 0) {
        Player player = findPlayerAfterButton(players.get(buttonPos).getSeatIndex(), winners);
        player.addChips(remainingWinnings);
        System.out.println("Remaining Chips added to " + player.getName());
      }
    });
  }

  private Player findPlayerAfterButton(int buttonSeatIndex, List<Player> players) {
    int lowestIndex = Integer.MAX_VALUE;
    if (buttonSeatIndex == players.size() - 1) {
      buttonSeatIndex = -1; //put at start (so its looking for index 0 as left of button)
    }
    for (Player player : players) {
      int playerSeatIndex = player.getSeatIndex();
      if (playerSeatIndex > buttonSeatIndex && playerSeatIndex < lowestIndex) {
        lowestIndex = playerSeatIndex;
      }

      if (lowestIndex - 1 == buttonSeatIndex) {
        return player;
      }
    }

    return players.get(lowestIndex);
  }

  private void printGameState() {
    System.out.println(createGameStateString());
  }

  private String createGameStateString() {
    int indexOfActionPos = indexOfRealActionPos();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < players.size(); i++) {
      Player p = players.get(i);
      String playerName = p.getName();
      if (buttonPos == i) {
        playerName += '*';
      }

      char actionChar = ' ';
      if (indexOfActionPos == i) {
        actionChar = '>';
      }

      Result r = Result.calculate(p.getHand(), board.toArray(new Card[0]), GameType.TEXAS_HOLDEM);
      if (p.isFolded()) {
        sb.append(String.format("%c %-5s: %s stack: %-4d bet: %d (%s) folded%n",
            actionChar, playerName, p.getHand(), p.getChips(), p.getCurrentBet(),
            r.getRankingVerbose()));
      } else {
        sb.append(String.format("%c %-5s: %s stack: %-4d bet: %d (%s)%n",
            actionChar, playerName, p.getHand(), p.getChips(), p.getCurrentBet(),
            r.getRankingVerbose()));
      }
    }

    sb.append('\n').append("Board: ").append(board).append(", pot: ").append(potManager);
    return sb.toString();
  }

  private Map<Pot, List<Player>> determineWinners() {
    System.out.println("Results: ");
    players.stream()
        .peek(player -> player.determineResult(board))
        .forEach(player -> System.out.println(player.getName() + ": " + player.getHand() + ", " + board + " = " + player.getResult().getCards() + " (" + player.getResult().getRankingString() + ")"));

    Map<Pot, List<Player>> potWinnersMap = new LinkedHashMap<>(potManager.sidePotsCount() + 1);
    List<Player> winningPlayers = determinePotWinners(potManager.getMainPot());
    potWinnersMap.put(potManager.getMainPot(), winningPlayers);

    Set<Player> winnersList = new HashSet<>(winningPlayers);

    List<Pot> sidePots = potManager.getSidePots();
    for (Pot sidePot : sidePots) {
      List<Player> tmpWinners = determinePotWinners(sidePot);
      potWinnersMap.put(sidePot, tmpWinners);
      winnersList.addAll(tmpWinners);
    }

    System.out.println("Winners - ");
    for (Player player : winnersList) {
      System.out.println(player.getName() + ": " + player.getHand() + ", " + board + " = " + player.getResult().getCards() + " (" + player.getResult().getRankingString() + ")");
    }

    return potWinnersMap;
  }

  private List<Player> determinePotWinners(Pot pot) {
    List<Player> winners = new ArrayList<>();
    Result topResult = null;
    for (Player player : pot.getPlayers()) {
      if (player.isFolded()) {
        continue;
      }
      Result result = player.getResult();
      if (result == null) {
        result = player.determineResult(board);
      }

      if (topResult == null) {
        topResult = result;
        winners.add(player);
        continue;
      }

      int compareResult = result.compareTo(topResult);
       if (compareResult > 0) {
         topResult = result;
         winners.clear();
         winners.add(player);
      } else if (compareResult == 0) {
         winners.add(player);
       }
    }

    return winners;
  }

  private int increment(int value, int incAmount) {
    int playersInHandCount = playersInHand.size();

    int result = (value + incAmount);
    if (result < 0) {
      return playersInHandCount - 1;
    }

    return result % playersInHandCount;
  }

  private int indexOfRealActionPos() {
    int index = 0;
    Player p = playersInHand.get(actionPos);
    while (index < players.size()) {
      if (p.getName().equals(players.get(index).getName())) {
        return index;
      }
      index++;
    }

    return -1; //assumed dead
  }

  private String getStageString(int stage) {
    switch (stage) {
      case PRE_FLOP: return "pre-flop";
      case FLOP: return "flop";
      case TURN: return "turn";
      case RIVER: return "river";
      default: throw new RuntimeException("Invalid action stage " + stage);
    }
  }


  private void dumpHand(Map<Integer, List<PlayerAction>> playerActionsMap, Map<Pot, List<Player>> potWinnersMap) {
      String filename = "hand_" + UUID.randomUUID() + ".log";
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
        writer.write(createGameStateString());

        writer.write("\nPre-flop actions: " + playerActionsMap.getOrDefault(PRE_FLOP, new ArrayList<>(0)));
        writer.write("\nFlop actions: " + playerActionsMap.getOrDefault(FLOP, new ArrayList<>(0)));
        writer.write("\nTurn actions: " + playerActionsMap.getOrDefault(TURN, new ArrayList<>(0)));
        writer.write("\nRiver actions: " + playerActionsMap.getOrDefault(RIVER, new ArrayList<>(0)));

        AtomicInteger remainderAtomic = new AtomicInteger();

        Map<Player, Integer> potShareMap = new HashMap<>();
        potWinnersMap.forEach((pot, winners) -> {
          int baseWinnings = pot.getChips() / winners.size();
          int remainingWinnings = pot.getChips() % winners.size();
          remainderAtomic.addAndGet(remainingWinnings);

          for (Player player : winners) {
            if (player.isFolded()) {
              continue;
            }

            int currentWinnings = potShareMap.getOrDefault(player, 0);
            potShareMap.put(player, currentWinnings + baseWinnings);
          }
        });

        writer.write("\n\nWinner(s): " + potShareMap.entrySet().stream()
            .map(entry -> entry.getKey().getName() + "(" + entry.getValue() + ")")
            .reduce((prev, next) -> prev + ", " + next)
            .orElse("None"));

        int remainder = remainderAtomic.get();
        if (remainder > 0) {
          writer.write("\n\nRemainder that needs to be shared: " + remainder);
        }

        System.out.println("Log file created: " + filename);
      } catch (Exception e) {
        e.printStackTrace();
      }

  }
}
