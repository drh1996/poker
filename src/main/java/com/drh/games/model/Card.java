package com.drh.games.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Card implements Comparable<Card> {
  public static final int ACE = 1;
  public static final int TEN = 10;
  public static final int JACK = 11;
  public static final int QUEEN = 12;
  public static final int KING = 13;

  public static final int SPADE = 0;
  public static final int HEART = 1;
  public static final int DIAMOND = 2;
  public static final int CLUB = 3;

  private static final int ACE_HIGH = KING + 1;
  private static final int ASCII_NUM_OFFSET = 48;

  private final int value;
  private final int suit;

  public char getSuitChar() {
    switch (suit) {
      case SPADE:   return 'S';
      case HEART:   return 'H';
      case DIAMOND: return 'D';
      case CLUB:    return 'C';
      default: throw new IllegalArgumentException("Invalid suit '" + (char) suit + '\'');
    }
  }

  public char getValueChar() {
    if (value < ACE || value > KING) {
      throw new IllegalArgumentException("Invalid value '" + value + '\'');
    }

    switch (value) {
      case ACE:   return 'A';
      case TEN:   return 'T';
      case JACK:  return 'J';
      case QUEEN: return 'Q';
      case KING:  return 'K';
      default:    return (char) (value + ASCII_NUM_OFFSET);
    }
  }

  public String getValueString() {
    if (value < ACE || value > KING) {
      throw new IllegalArgumentException("Invalid value '" + value + '\'');
    }

    switch (value) {
      case ACE:   return "Ace";
      case JACK:  return "Jack";
      case QUEEN: return "Queen";
      case KING:  return "King";
      default:    return String.valueOf(value);
    }
  }

  @Override
  public String toString() {
    return new StringBuilder(2)
        .append(getValueChar())
        .append(getSuitChar())
        .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Card)) {
      return false;
    }

    Card other = (Card) obj;

    return this.value == other.value && this.suit == other.suit;
  }

  @Override
  public int compareTo(Card o) {
    int leftValue = this.value == ACE ? ACE_HIGH : this.value;
    int rightValue = o.value == ACE ? ACE_HIGH : o.value;
    return Integer.compare(leftValue, rightValue);
  }
}
