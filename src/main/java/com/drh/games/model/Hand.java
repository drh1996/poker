package com.drh.games.model;

import java.util.Arrays;
import java.util.Collections;
import lombok.Getter;

@Getter
public class Hand {

  private final Card[] cards;

  public Hand(Card... cards) {
    this.cards = Arrays.copyOf(cards, cards.length);
    Arrays.sort(this.cards, Collections.reverseOrder());
  }

  @Override
  public String toString() {
    return Arrays.toString(cards);
  }
}
