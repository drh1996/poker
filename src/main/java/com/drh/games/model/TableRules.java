package com.drh.games.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TableRules {
  private final int bigBlind;
  private final int smallBlind;
}
