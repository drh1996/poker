package com.drh.games.model;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

public class Deck {

  private static final int DECK_SIZE = 52;
  private static final int VALUE_COUNT = Card.KING;

  private final Random random;
  private final Card[] cards;
  private int cardIndex;

  public Deck() {
    random = new SecureRandom();
    cards = createCards();
    cardIndex = 0;
  }

  public void shuffle() {
    for (int i = 0; i < cards.length; i++) {
      int newPos = random.nextInt(DECK_SIZE);

      Card tmp = cards[i];
      cards[i] = cards[newPos];
      cards[newPos] = tmp;
    }
    cardIndex = 0;
  }

  public void giveCards(Player player, GameType gameType) {
    player.setHand(getCards(gameType));
  }

  public Card[] getCards(GameType gameType) {
    Card[] playerCards = new Card[gameType.getCardsPerHand()];
    for (int i = 0; i < playerCards.length; i++) {
      playerCards[i] = getCard();
    }
    return playerCards;
  }

  public Card getCard() {
    if (cards.length == cardIndex) {
      throw new IllegalStateException("No cards left");
    }
    return cards[cardIndex++];
  }

  private static Card[] createCards() {
    Card[] cards = new Card[DECK_SIZE];
    for (int value = Card.ACE; value <= Card.KING; value++) {
      for (int suit = Card.SPADE; suit <= Card.CLUB; suit++) {
        cards[value - 1 + suit * VALUE_COUNT] = new Card(value, suit);
      }
    }
    return cards;
  }

  @Override
  public String toString() {
    return Arrays.toString(cards);
  }
}
