package com.drh.games.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum GameType {
  TEXAS_HOLDEM(2, 9),
  OMAHA_HOLDEM(4, 9);

  private final int cardsPerHand;
  private final int maxPlayersPerTable;
}
