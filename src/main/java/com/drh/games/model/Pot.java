package com.drh.games.model;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class Pot {

  private int chips;
  @Setter
  private int maxChipsPerPlayer;
  @Setter
  private boolean locked;
  private final List<Player> players;

  public Pot() {
    this.chips = 0;
    this.players = new ArrayList<>();
  }

  public void addChips(int chips, Player player) {
    this.chips += chips;
    if (!players.contains(player)) {
      this.players.add(player);
    }
  }

  public void reset() {
    this.chips = this.maxChipsPerPlayer = 0;
    this.players.clear();
    this.locked = false;
  }
}
