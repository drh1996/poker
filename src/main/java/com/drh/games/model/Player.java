package com.drh.games.model;

import com.drh.games.result.Result;
import java.util.List;
import java.util.Scanner;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@RequiredArgsConstructor
public class Player {

  public static final int NONE = 0;
  public static final int BUTTON = 1;
  public static final int SMALL_BLIND = 2;
  public static final int BIG_BLIND = 4;


  private static final Scanner scanner = new Scanner(System.in);

  private final String name;
  private final int seatIndex;
  private int chips = 100;
  private int currentBet;
  @Setter
  private boolean folded;
  private boolean allIn;
  private Hand hand;
  private Result result;
  private int handFlags;

  public void setHand(Card... cards) {
    hand = new Hand(cards);
  }

  public void clearHand() {
    hand = null;
    result = null;
    currentBet = 0;
    allIn = folded = false;
    handFlags = NONE;
  }

  public boolean isButton() {
    return (handFlags & BUTTON) == BUTTON;
  }

  public boolean isSmallBlind() {
    return (handFlags & SMALL_BLIND) == SMALL_BLIND;
  }

  public boolean isBigBlind() {
    return (handFlags & BIG_BLIND) == BIG_BLIND;
  }

  public int paySmallBlind(int blind) {
    addFlag(SMALL_BLIND);
    return payBlind(blind);
  }

  public int payBigBlind(int blind) {
    addFlag(BIG_BLIND);
    return payBlind(blind);
  }

  private int payBlind(int blind) {
    if (blind >= chips) {
      blind = chips;
      allIn = true;
    }

    chips -= blind;
    currentBet += blind;

    return blind;
  }

  public Result determineResult(List<Card> board) {
    return result = Result.calculate(hand, board.toArray(new Card[0]), GameType.TEXAS_HOLDEM);
  }

  public void setCurrentBet(int bet) {
    if (bet >= chips) {
      bet = chips;
      allIn = true;
    }

    chips -= (bet - currentBet);
    currentBet = bet;
  }

  public void resetBet() {
    currentBet = 0;
  }

  public void addChips(int chips) {
    this.chips += chips;
  }

  public void resetFlags() {
    handFlags = NONE;
  }

  public void addFlag(int flag) {
    handFlags = handFlags | flag;
  }

}
