package com.drh.games.model;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class DeckTest {

  @Test
  public void test_shuffle_doesNotThrow() {
    Deck d = new Deck();
    assertDoesNotThrow(d::shuffle);
  }

  @Test
  public void test_giveCardsToPlayer() {
    Player player = new Player("Test");
    Deck deck = new Deck();

    deck.giveCards(player, GameType.TEXAS_HOLDEM);
    assertEquals(GameType.TEXAS_HOLDEM.getCardsPerHand(), player.getHand().getCards().length);

    deck.giveCards(player, GameType.OMAHA_HOLDEM);
    assertEquals(GameType.OMAHA_HOLDEM.getCardsPerHand(), player.getHand().getCards().length);
  }

  @Test
  public void test_getCard_depletedDeck() {
    Deck deck = new Deck();
    for (int i = 0; i < 52; i++) {
      deck.getCard();
    }

    IllegalStateException e = assertThrows(IllegalStateException.class, deck::getCard);
    assertEquals("No cards left",e.getMessage());
  }

  @Test
  public void test_toString() {
    String expected = "[AS, 2S, 3S, 4S, 5S, 6S, 7S, 8S, 9S, TS, JS, QS, KS, "
        + "AH, 2H, 3H, 4H, 5H, 6H, 7H, 8H, 9H, TH, JH, QH, KH, "
        + "AD, 2D, 3D, 4D, 5D, 6D, 7D, 8D, 9D, TD, JD, QD, KD, "
        + "AC, 2C, 3C, 4C, 5C, 6C, 7C, 8C, 9C, TC, JC, QC, KC]";

    Deck d = new Deck();

    assertEquals(expected, d.toString());
  }
}
