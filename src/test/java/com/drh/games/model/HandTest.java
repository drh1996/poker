package com.drh.games.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

public class HandTest {

  @Test
  public void test_handIsOrdered() {
    Card c1 = new Card(4, Card.SPADE);
    Card c2 = new Card(Card.ACE, Card.DIAMOND);
    Card c3 = new Card(Card.JACK, Card.SPADE);
    Card c4 = new Card(2, Card.CLUB);

    final Card[] expected = new Card[] {c2, c3, c1, c4};
    Hand h = new Hand(c1, c2, c3, c4);

    assertArrayEquals(expected, h.getCards());
  }

  @Test
  public void test_toString() {
    Card c1 = new Card(4, Card.SPADE);
    Card c2 = new Card(Card.ACE, Card.DIAMOND);
    Card c3 = new Card(Card.JACK, Card.SPADE);
    Card c4 = new Card(2, Card.CLUB);

    Hand h = new Hand(c1, c2, c3, c4);

    assertEquals("[AD, JS, 4S, 2C]", h.toString());
  }

  @Test
  public void test_cardsOrderImmutable() {
    Card c1 = new Card(4, Card.SPADE);
    Card c2 = new Card(Card.ACE, Card.DIAMOND);
    Card c3 = new Card(Card.JACK, Card.SPADE);
    Card c4 = new Card(2, Card.CLUB);

    final Card[] immutableArr = new Card[] {c1, c2, c3, c4};
    Hand h = new Hand(immutableArr);

    assertArrayEquals(new Card[] {c1, c2, c3, c4}, immutableArr);
    assertNotEquals(immutableArr, h.getCards()); //test copy array was made by reference
  }
}
