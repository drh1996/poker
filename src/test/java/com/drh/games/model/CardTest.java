package com.drh.games.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class CardTest {

  @Test
  public void test_getSuit() {
    Card c = new Card(Card.ACE, Card.SPADE);
    assertEquals(Card.SPADE, c.getSuit());

    c = new Card(Card.ACE, Card.HEART);
    assertEquals(Card.HEART, c.getSuit());

    c = new Card(Card.ACE, Card.DIAMOND);
    assertEquals(Card.DIAMOND, c.getSuit());

    c = new Card(Card.ACE, Card.CLUB);
    assertEquals(Card.CLUB, c.getSuit());
  }

  @Test
  public void test_getSuitChar() {
    Card c = new Card(Card.ACE, Card.SPADE);
    assertEquals('S', c.getSuitChar());

    c = new Card(Card.ACE, Card.HEART);
    assertEquals('H', c.getSuitChar());

    c = new Card(Card.ACE, Card.DIAMOND);
    assertEquals('D', c.getSuitChar());

    c = new Card(Card.ACE, Card.CLUB);
    assertEquals('C', c.getSuitChar());

    c = new Card(Card.ACE, 'A');
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, c::getSuitChar);
    assertEquals("Invalid suit 'A'", e.getMessage());
  }

  @Test
  public void test_getValue() {
    Card c = new Card(Card.ACE, Card.SPADE);
    assertEquals(Card.ACE, c.getValue());

    c = new Card(Card.TEN, Card.SPADE);
    assertEquals(Card.TEN, c.getValue());

    c = new Card(Card.JACK, Card.SPADE);
    assertEquals(Card.JACK, c.getValue());

    c = new Card(Card.QUEEN, Card.SPADE);
    assertEquals(Card.QUEEN, c.getValue());

    c = new Card(Card.KING, Card.SPADE);
    assertEquals(Card.KING, c.getValue());

    c = new Card(2, Card.SPADE);
    assertEquals(2, c.getValue());
  }

  @Test
  public void test_getValueChar() {
    Card c = new Card(Card.ACE, Card.SPADE);
    assertEquals('A', c.getValueChar());

    c = new Card(Card.TEN, Card.SPADE);
    assertEquals('T', c.getValueChar());

    c = new Card(Card.JACK, Card.SPADE);
    assertEquals('J', c.getValueChar());

    c = new Card(Card.QUEEN, Card.SPADE);
    assertEquals('Q', c.getValueChar());

    c = new Card(Card.KING, Card.SPADE);
    assertEquals('K', c.getValueChar());

    c = new Card(2, Card.SPADE);
    assertEquals('2', c.getValueChar());

    c = new Card(0, Card.SPADE);
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, c::getValueChar);
    assertEquals("Invalid value '0'", e.getMessage());
  }

  @Test
  public void test_getValueString() {
    Card c = new Card(Card.ACE, Card.SPADE);
    assertEquals("Ace", c.getValueString());

    c = new Card(Card.JACK, Card.SPADE);
    assertEquals("Jack", c.getValueString());

    c = new Card(Card.QUEEN, Card.SPADE);
    assertEquals("Queen", c.getValueString());

    c = new Card(Card.KING, Card.SPADE);
    assertEquals("King", c.getValueString());

    c = new Card(2, Card.SPADE);
    assertEquals("2", c.getValueString());

    c = new Card(0, Card.SPADE);
    IllegalArgumentException e = assertThrows(IllegalArgumentException.class, c::getValueString);
    assertEquals("Invalid value '0'", e.getMessage());
  }

  @Test
  public void test_equals() {
    Card c = new Card(Card.ACE, Card.SPADE);
    Card c2 = new Card(Card.ACE, Card.DIAMOND);
    Card c3 = new Card(Card.KING, Card.DIAMOND);
    Card c4 = new Card(Card.KING, Card.SPADE);
    Card c5 = new Card(Card.ACE, Card.SPADE);

    assertTrue(c.equals(c));
    assertTrue(c.equals(c5));

    assertFalse(c.equals(c2));
    assertFalse(c.equals(c3));
    assertFalse(c.equals(c4));
    assertFalse(c.equals(null));
    assertFalse(c.equals("Test"));
  }

  @Test
  public void test_compareTo() {
    Card c = new Card(Card.ACE, Card.SPADE);
    Card c2 = new Card(Card.ACE, Card.DIAMOND);
    Card c3 = new Card(Card.KING, Card.DIAMOND);

    assertEquals(0, c.compareTo(c));

    assertEquals(0, c.compareTo(c2));
    assertEquals(0, c2.compareTo(c));

    assertEquals(1, c.compareTo(c3));
    assertEquals(-1, c3.compareTo(c));
  }

  @Test
  public void test_toString() {
    Card c = new Card(Card.ACE, Card.SPADE);
    Card c2 = new Card(Card.KING, Card.DIAMOND);

    assertEquals("AS", c.toString());
    assertEquals("KD", c2.toString());
  }
}
