package com.drh.games.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import com.drh.games.model.Player;
import com.drh.games.model.PlayerAction;
import com.drh.games.model.TableRules;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TerminalPlayerActionRequesterTest {

  private static final String LINE_SEPARATOR = System.lineSeparator();

  private PlayerActionRequester playerActionRequester;

  @BeforeEach
  public void beforeEach() {
    playerActionRequester = TerminalPlayerActionRequester.instance();
  }

  @Test
  public void test_requestAction_invalidAction_thenValidAction() {
    loadUserInput("invalid_action", "fold");

    List<PlayerAction> prevActions = new ArrayList<>();
    Player player = new Player("Test");
    TableRules tableRules = new TableRules(2, 1);

    PlayerAction actual = assertDoesNotThrow(
        () -> playerActionRequester.requestAction(prevActions, player, tableRules, false));

    assertEquals(PlayerAction.FOLD, actual.getAction());
    assertEquals(0, actual.getChips());
  }

  private void loadUserInput(String... lines) {
    StringBuilder sb = new StringBuilder();
    for (String line : lines) {
      sb.append(line).append(LINE_SEPARATOR);
    }

    try {
      Field scannerField = TerminalPlayerActionRequester.class.getDeclaredField("scanner");
      scannerField.setAccessible(true);
      scannerField.set(playerActionRequester, new Scanner(new ByteArrayInputStream(sb.toString().getBytes())));
    } catch (NoSuchFieldException | IllegalAccessException e) {
      fail("Failed to load user data", e);
    }
  }
}
