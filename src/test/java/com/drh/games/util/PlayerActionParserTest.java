package com.drh.games.util;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.drh.games.model.Player;
import com.drh.games.model.PlayerAction;
import com.drh.games.model.TableRules;
import com.drh.games.support.TestSupportUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlayerActionParserTest {

  private Player player;
  private TableRules tableRules;

  @BeforeEach
  public void beforeEach() throws NoSuchFieldException, IllegalAccessException {
    player = new Player("Test");
    TestSupportUtils.setFieldValue(Player.class, player, "chips", 100);
    tableRules = new TableRules(2, 1);
  }

  @Test
  public void test_parseAndValidateAction_happyPathSuccess() {
    PlayerAction actual = PlayerActionParser.parseAndValidateAction(
        "fold", emptyList(), player, tableRules, false);
    assertEquals(PlayerAction.FOLD, actual.getAction());
    assertEquals(0, actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "check", emptyList(), player, tableRules, false);
    assertEquals(PlayerAction.CHECK, actual.getAction());
    assertEquals(0, actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "call", singletonList(new PlayerAction(player, PlayerAction.BET, 10)), player, tableRules, false);
    assertEquals(PlayerAction.CALL, actual.getAction());
    assertEquals(10, actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "bet 10", emptyList(), player, tableRules, false);
    assertEquals(PlayerAction.BET, actual.getAction());
    assertEquals(10, actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "raise 20", singletonList(new PlayerAction(player, PlayerAction.BET, 10)), player, tableRules, false);
    assertEquals(PlayerAction.RAISE, actual.getAction());
    assertEquals(20, actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "all in", singletonList(new PlayerAction(player, PlayerAction.BET, 10)), player, tableRules, false);
    assertEquals(PlayerAction.ALL_IN, actual.getAction());
    assertEquals(player.getChips(), actual.getChips());

    actual = PlayerActionParser.parseAndValidateAction(
        "bet 100", emptyList(), player, tableRules, false);
    assertEquals(PlayerAction.ALL_IN, actual.getAction());
    assertEquals(player.getChips(), actual.getChips());
  }

  @Test
  public void test_parseAndValidateAction_invalidActionUserInput() {
    IllegalArgumentException eRegexNoMatch = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("%£$(", null, null, null, false));

    assertEquals("Invalid action provided '%£$('", eRegexNoMatch.getMessage());

    IllegalArgumentException eInvalidAction = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("flip table", null, null, null, false));

    assertEquals("Player action 'flip table' is not valid", eInvalidAction.getMessage());
  }

  @Test
  public void test_parseAndValidateAction_invalidActionUsage() {
    IllegalArgumentException eCallFirst = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("call", emptyList(), null, null, false));

    assertEquals("First to act cannot call", eCallFirst.getMessage());

    IllegalArgumentException eRaiseFirst = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("raise 10", emptyList(), null, null, false));

    assertEquals("First to act cannot raise", eRaiseFirst.getMessage());

    IllegalArgumentException eCheckWhenPrevBet = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("check", singletonList(new PlayerAction(player, PlayerAction.BET, 10)), null, null, false));

    assertEquals("You cannot check because the previous player bet", eCheckWhenPrevBet.getMessage());

    IllegalArgumentException eCallWhenPrevCheck = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("call", singletonList(new PlayerAction(player, PlayerAction.CHECK)), null, null, false));

    assertEquals("You cannot call because the previous player checked", eCallWhenPrevCheck.getMessage());

    IllegalArgumentException eBetWhenPrevRaise = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("bet 10", singletonList(new PlayerAction(player, PlayerAction.RAISE, 10)), null, null, false));

    assertEquals("You cannot bet because the previous player raised", eBetWhenPrevRaise.getMessage());

    IllegalArgumentException eRaiseWhenPrevCheck = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("raise 20", singletonList(new PlayerAction(player, PlayerAction.CHECK)), null, null, false));

    assertEquals("You cannot raise because the previous player checked", eRaiseWhenPrevCheck.getMessage());


  }

  @Test
  public void test_parseAndValidateAction_invalidChipsUserInput() {
    IllegalArgumentException eNoChips = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("bet", emptyList(), null, null, false));

    assertEquals("No chips provided for bet", eNoChips.getMessage());


    IllegalArgumentException eInvalidChips = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("bet 123123123123123123", emptyList(), null, null, false));

    assertEquals("Chips provided are not valid '123123123123123123'", eInvalidChips.getMessage());
  }

  @Test
  public void test_parseAndValidateAction_invalidChipsUsage() {
    IllegalArgumentException eNotEnoughChips = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("bet 9999", emptyList(), player, null, false));

    assertEquals("You only have 100 chips remaining", eNotEnoughChips.getMessage());

    IllegalArgumentException eLessThanMinBet = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("bet 1", emptyList(), player, tableRules, false));

    assertEquals("Cannot bet 1 because it's less than the minimum (2)", eLessThanMinBet.getMessage());

    IllegalArgumentException eLessThanMinRaise = assertThrows(IllegalArgumentException.class,
        () -> PlayerActionParser.parseAndValidateAction("raise 3", singletonList(new PlayerAction(player, PlayerAction.BET, 2)), player, tableRules, false));

    assertEquals("Cannot raise less than the minimum 4", eLessThanMinRaise.getMessage());


  }
}
