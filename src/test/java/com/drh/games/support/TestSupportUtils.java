package com.drh.games.support;

import static com.drh.games.model.Card.ACE;
import static com.drh.games.model.Card.CLUB;
import static com.drh.games.model.Card.DIAMOND;
import static com.drh.games.model.Card.HEART;
import static com.drh.games.model.Card.JACK;
import static com.drh.games.model.Card.KING;
import static com.drh.games.model.Card.QUEEN;
import static com.drh.games.model.Card.SPADE;
import static org.junit.jupiter.api.Assertions.fail;

import com.drh.games.model.Card;
import java.lang.reflect.Field;

public class TestSupportUtils {

  public static void setFieldValue(Class<?> cls, Object src, String fieldName, Object value)
      throws NoSuchFieldException, IllegalAccessException {

    Field scannerField = cls.getDeclaredField(fieldName);
    scannerField.setAccessible(true);
    scannerField.set(src, value);
  }

  public static Card[] fromString(String... strs) {
    Card[] cards = new Card[strs.length];
    for (int i = 0; i < cards.length; i++) {
      cards[i] = fromString(strs[i]);
    }
    return cards;
  }

  public static Card fromString(String str) {
    char value = str.charAt(0);
    char suit = str.charAt(1);

    int suitInt = -1, valueInt;

    switch (value) {
      case 'A': valueInt = ACE; break;
      case 'T': valueInt = 10; break;
      case 'J': valueInt = JACK; break;
      case 'Q': valueInt = QUEEN; break;
      case 'K': valueInt = KING; break;
      default : valueInt = value - 48;
    }

    switch (suit) {
      case 'S': suitInt = SPADE; break;
      case 'H': suitInt = HEART; break;
      case 'D': suitInt = DIAMOND; break;
      case 'C': suitInt = CLUB; break;
    }

    if (valueInt < 1 || valueInt > 13 || suitInt == -1) {
      fail(String.format("value %s or suit %s is invalid", value, suit));
    }

    return new Card(valueInt, suitInt);
  }
}
