package com.drh.games.result.calculator;

import static com.drh.games.result.Result.FLUSH;
import static com.drh.games.result.Result.FOUR_OF_A_KIND;
import static com.drh.games.result.Result.FULL_HOUSE;
import static com.drh.games.result.Result.HIGH_CARD;
import static com.drh.games.result.Result.PAIR;
import static com.drh.games.result.Result.ROYAL_FLUSH;
import static com.drh.games.result.Result.STRAIGHT;
import static com.drh.games.result.Result.STRAIGHT_FLUSH;
import static com.drh.games.result.Result.THREE_OF_A_KIND;
import static com.drh.games.result.Result.TWO_PAIR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import com.drh.games.model.Card;
import com.drh.games.model.GameType;
import com.drh.games.model.Hand;
import com.drh.games.support.TestSupportUtils;
import com.drh.games.result.Result;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

public class TexasHoldemResultCalculatorTest {

  private static final Pattern INPUT_PATTERN = Pattern.compile("^\\[([0-9TJQKAHDSC,\\s]+)],\\s*\\[([0-9TJQKAHDSC,\\s]+)\\]\\s*=\\s*\\[([0-9TJQKAHDSC,\\s]+)\\]\\s*\"([\\w\\s]+)\"$");

  @Test
  public void testFromFile() {
    for (TestData testData : Objects.requireNonNull(createTestData())) {
      testData.doTest();
    }
  }

  private List<TestData> createTestData() {
    try (Stream<String> stream = Files.lines(Path.of("src/test/resources/data/test-cards.txt"))){
      return stream.filter(s -> !s.isBlank() && !s.startsWith("#"))
          .map(TestData::constructTestData)
          .collect(Collectors.toList());
    } catch (IOException e) {
      e.printStackTrace();
      fail(e.getMessage());
    }
    return null;
  }

  @RequiredArgsConstructor
  private static class TestData {
    final Hand hand;
    final Card[] cards;
    final Result expected;
    final String expectedRankingStr;

    public static TestData constructTestData(String s) {
      Matcher matcher = INPUT_PATTERN.matcher(s);

      if (!matcher.matches()) {
        fail("Test string '" + s + "' is not valid");
      }

      Hand hand = new Hand(constructCards(matcher.group(1)));
      Card[] cards = constructCards(matcher.group(2));
      Card[] expectedCards = constructCards(matcher.group(3));
      String expectedRankingStr = matcher.group(4);
      int expectedRanking = calculateRanking(expectedRankingStr);

      return new TestData(hand, cards, new Result(expectedRanking, Arrays.asList(expectedCards)), expectedRankingStr);
    }

    public void doTest() {
      Result actual = Result.calculate(hand, cards, GameType.TEXAS_HOLDEM);

      System.out.printf("Test For %s, %s\n   Expected = %s (%s)\n   Actual   = %s (%s) %s\n\n",
          hand, Arrays.toString(cards), expected.getCards(), expectedRankingStr, actual.getCards(), actual.getRankingString(), actual.getRankingVerbose());

      assertEquals(expected.getCards(), actual.getCards(), "Cards not correct");
      assertEquals(expected.getRanking(), actual.getRanking(), "Ranking not correct");
    }

    private static Card[] constructCards(String src) {
      String[] strs = src.split(",\\s*");

      Card[] cards = new Card[strs.length];
      for (int i = 0; i < strs.length; i++) {
        cards[i] = TestSupportUtils.fromString(strs[i]);
      }
      return cards;
    }

    public static int calculateRanking(String ranking) {
      switch (ranking.toLowerCase()) {
        case "royal flush": return ROYAL_FLUSH;
        case "straight flush": return STRAIGHT_FLUSH;
        case "four of a kind": return FOUR_OF_A_KIND;
        case "full house": return FULL_HOUSE;
        case "flush": return FLUSH;
        case "straight": return STRAIGHT;
        case "three of a kind": return THREE_OF_A_KIND;
        case "two pair": return TWO_PAIR;
        case "pair": return PAIR;
        case "high card": return HIGH_CARD;
        default: fail(String.format("invalid expected rank provided '%s'", ranking));
      }
      return -1;
    }
  }
}
