package com.drh.games.result;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.drh.games.model.Card;
import com.drh.games.model.GameType;
import com.drh.games.model.Hand;
import com.drh.games.support.TestSupportUtils;
import java.util.Arrays;
import org.junit.jupiter.api.Test;

public class ResultTest {

  @Test
  public void test_calculate_texasHoldem() {
    Hand hand = new Hand(TestSupportUtils.fromString("AS", "2H"));
    Card[] cards = TestSupportUtils.fromString("9H", "3D", "4D", "KS", "5D");

    Result expected = new Result(Result.STRAIGHT, Arrays.asList(TestSupportUtils.fromString(
        "AS", "2H", "3D", "4D", "5D")));

    Result actual = assertDoesNotThrow(() -> Result.calculate(hand, cards, GameType.TEXAS_HOLDEM));

    assertEquals(expected.getRanking(), actual.getRanking());
    assertArrayEquals(expected.getCards().toArray(new Card[0]), actual.getCards().toArray(new Card[0]));
  }

  @Test
  public void test_compareTo() {
    Card[] cards1 = TestSupportUtils.fromString("AS", "2H", "3D", "4D", "5D");
    Card[] cards2 = TestSupportUtils.fromString("2H", "3D", "4D", "5D", "6S");

    Result result1 = new Result(Result.STRAIGHT, Arrays.asList(cards1));
    Result result2 = new Result(Result.STRAIGHT, Arrays.asList(cards2));

    assertEquals(-1, result1.compareTo(result2));
  }

  @Test
  public void test_ResultIllegalRanking() {
    Result r = new Result(-1, null);

    IllegalArgumentException eRankingStr = assertThrows(IllegalArgumentException.class, r::getRankingString);
    assertEquals("Invalid ranking '-1'", eRankingStr.getMessage());

    IllegalArgumentException eRankingVerbose = assertThrows(IllegalArgumentException.class, r::getRankingVerbose);
    assertEquals("Invalid ranking '-1'", eRankingVerbose.getMessage());
  }

  @Test
  public void test_calculateResult_UnsupportedGameType() {
    UnsupportedOperationException eGameType = assertThrows(UnsupportedOperationException.class, () -> Result.calculate(new Hand(), new Card[0], GameType.OMAHA_HOLDEM));
    assertEquals("Unsupported Game Type: " + GameType.OMAHA_HOLDEM, eGameType.getMessage());
  }

  @Test
  public void test_calculateResult_NullArgs() {
    NullPointerException eNullHand = assertThrows(NullPointerException.class, () -> Result.calculate(null, null, null));
    assertEquals("hand cannot be null", eNullHand.getMessage());

    NullPointerException eNullCards = assertThrows(NullPointerException.class, () -> Result.calculate(new Hand(), null, null));
    assertEquals("cards cannot be null", eNullCards.getMessage());

    NullPointerException eNullGameType = assertThrows(NullPointerException.class, () -> Result.calculate(new Hand(), new Card[0], null));
    assertEquals("gameType cannot be null", eNullGameType.getMessage());
  }
}
