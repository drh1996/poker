package com.drh.games.result.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.drh.games.model.Card;
import com.drh.games.result.Result;
import com.drh.games.result.util.ResultStringUtils;
import com.drh.games.support.TestSupportUtils;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

public class ResultStringUtilsTest {

  @Test
  public void test_getRankingString() {
    assertEquals("Royal Flush", ResultStringUtils.getRankingString(Result.ROYAL_FLUSH));
    assertEquals("Straight Flush", ResultStringUtils.getRankingString(Result.STRAIGHT_FLUSH));
    assertEquals("Four Of A Kind", ResultStringUtils.getRankingString(Result.FOUR_OF_A_KIND));
    assertEquals("Full House", ResultStringUtils.getRankingString(Result.FULL_HOUSE));
    assertEquals("Flush", ResultStringUtils.getRankingString(Result.FLUSH));
    assertEquals("Straight", ResultStringUtils.getRankingString(Result.STRAIGHT));
    assertEquals("Three Of A Kind", ResultStringUtils.getRankingString(Result.THREE_OF_A_KIND));
    assertEquals("Two Pair", ResultStringUtils.getRankingString(Result.TWO_PAIR));
    assertEquals("Pair", ResultStringUtils.getRankingString(Result.PAIR));
    assertEquals("High Card", ResultStringUtils.getRankingString(Result.HIGH_CARD));

    IllegalArgumentException e =assertThrows(IllegalArgumentException.class,
        () -> ResultStringUtils.getRankingString(-1));

    assertEquals("Invalid ranking '-1'", e.getMessage());
  }

  @Test
  public void test_getRankingVerbose() {
    assertEquals("Royal Flush", ResultStringUtils.getRankingVerbose(Result.ROYAL_FLUSH, null));
    assertEquals("Straight Flush Ace to 5", ResultStringUtils.getRankingVerbose(Result.STRAIGHT_FLUSH, getStraightFlushCards()));
    assertEquals("Four Of A Kind Aces", ResultStringUtils.getRankingVerbose(Result.FOUR_OF_A_KIND, getFourKindCards()));
    assertEquals("Full House Aces and 5s", ResultStringUtils.getRankingVerbose(Result.FULL_HOUSE, getFullHouseCards()));
    assertEquals("Ace High Flush", ResultStringUtils.getRankingVerbose(Result.FLUSH, getFlushCards()));
    assertEquals("Straight Ace to 5", ResultStringUtils.getRankingVerbose(Result.STRAIGHT, getStraightCards()));
    assertEquals("Three Of A Kind Aces", ResultStringUtils.getRankingVerbose(Result.THREE_OF_A_KIND, getThreeKindCards()));
    assertEquals("Two Pair Aces and 5s", ResultStringUtils.getRankingVerbose(Result.TWO_PAIR, getTwoPairCards()));
    assertEquals("Pair of Aces", ResultStringUtils.getRankingVerbose(Result.PAIR, getPairCards()));
    assertEquals("Ace High", ResultStringUtils.getRankingVerbose(Result.HIGH_CARD, getHighCards()));

    IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
        () -> ResultStringUtils.getRankingVerbose(-1, null));

    assertEquals("Invalid ranking '-1'", e.getMessage());
  }

  private List<Card> getStraightFlushCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "2S", "3S", "4S", "5S"));
  }

  private List<Card> getFourKindCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "AH", "AD", "AC", "5D"));
  }

  private List<Card> getFullHouseCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "AH", "AD", "5H", "5D"));
  }

  private List<Card> getFlushCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "JS", "9S", "3S", "2S"));
  }

  private List<Card> getStraightCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "2H", "3D", "4D", "5D"));
  }

  private List<Card> getThreeKindCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "AH", "AD", "9H", "5D"));
  }

  private List<Card> getTwoPairCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "AH", "5H", "5D", "6D"));
  }

  private List<Card> getPairCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "AH", "9D", "6D", "5D"));
  }

  private List<Card> getHighCards() {
    return Arrays.asList(TestSupportUtils.fromString("AS", "JH", "9D", "6D", "5D"));
  }
}
