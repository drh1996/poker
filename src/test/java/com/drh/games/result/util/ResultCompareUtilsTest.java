package com.drh.games.result.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.drh.games.model.Card;
import com.drh.games.result.Result;
import com.drh.games.support.TestSupportUtils;
import java.util.Arrays;
import org.junit.jupiter.api.Test;

public class ResultCompareUtilsTest {

  @Test
  public void test_compareWinner_differentRanking() {
    Result highCardResult = new Result(Result.HIGH_CARD, null);
    Result pairResult = new Result(Result.PAIR, null);
    Result twoPairResult = new Result(Result.TWO_PAIR, null);
    Result threeKindResult = new Result(Result.THREE_OF_A_KIND, null);
    Result straightResult = new Result(Result.STRAIGHT, null);
    Result flushResult = new Result(Result.FLUSH, null);
    Result fullHouseResult = new Result(Result.FULL_HOUSE, null);
    Result fourKindResult = new Result(Result.FOUR_OF_A_KIND, null);
    Result straightFlushResult = new Result(Result.STRAIGHT_FLUSH, null);
    Result royalFlushResult = new Result(Result.ROYAL_FLUSH, null);

    assertEquals(1, ResultCompareUtils.compare(pairResult, highCardResult));
    assertEquals(1, ResultCompareUtils.compare(twoPairResult, pairResult));
    assertEquals(1, ResultCompareUtils.compare(threeKindResult, twoPairResult));
    assertEquals(1, ResultCompareUtils.compare(straightResult, threeKindResult));
    assertEquals(1, ResultCompareUtils.compare(flushResult, straightResult));
    assertEquals(1, ResultCompareUtils.compare(fullHouseResult, flushResult));
    assertEquals(1, ResultCompareUtils.compare(fourKindResult, fullHouseResult));
    assertEquals(1, ResultCompareUtils.compare(straightFlushResult, fourKindResult));
    assertEquals(1, ResultCompareUtils.compare(royalFlushResult, straightFlushResult));
  }

  @Test
  public void test_compareRoyalFlush() {
    Result royalFlushResult1 = new Result(Result.ROYAL_FLUSH, null);
    Result royalFlushResult2 = new Result(Result.ROYAL_FLUSH, null);
    assertEquals(0, ResultCompareUtils.compare(royalFlushResult1, royalFlushResult2));
  }

  @Test
  public void test_compareHighCards() {
    Card[] highCards1 = TestSupportUtils.fromString("AS", "JH", "9D", "6D", "5D");
    Card[] highCards2 = TestSupportUtils.fromString("KD","QS", "9D",  "8D", "4H");

    Result left = new Result(Result.HIGH_CARD, Arrays.asList(highCards1));
    Result right = new Result(Result.HIGH_CARD, Arrays.asList(highCards2));

    assertEquals(1, ResultCompareUtils.compare(left, right));
    assertEquals(-1, ResultCompareUtils.compare(right, left));
    assertEquals(0, ResultCompareUtils.compare(left, left));
  }

  @Test
  public void test_compareStraight() {
    Card[] straight1 = TestSupportUtils.fromString("9S", "TH", "JD", "QD", "KD");
    Card[] straight2 = TestSupportUtils.fromString("2D","3S", "4D",  "5D", "6H");

    Result left = new Result(Result.STRAIGHT, Arrays.asList(straight1));
    Result right = new Result(Result.STRAIGHT, Arrays.asList(straight2));

    assertEquals(1, ResultCompareUtils.compare(left, right));
  }

  @Test
  public void test_compareAceStraight() {
    Card[] straight1 = TestSupportUtils.fromString("AS", "2H", "3D", "4D", "5D");
    Card[] straight2 = TestSupportUtils.fromString("2D","3S", "4D",  "5D", "6H");

    Result left = new Result(Result.STRAIGHT, Arrays.asList(straight1));
    Result right = new Result(Result.STRAIGHT, Arrays.asList(straight2));

    assertEquals(-1, ResultCompareUtils.compare(left, right));
  }
}
